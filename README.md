# GitLab Code Quality

* [Code quality diff using Code Climate](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality_diff.html)
* [Analyze project code quality with Code Climate CLI](https://docs.gitlab.com/ce/ci/examples/code_climate.html)
